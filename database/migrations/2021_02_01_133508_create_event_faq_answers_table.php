<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventFaqAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_faq_answers', function (Blueprint $table) {
            $table->id();
            $table->longText('answer');
            $table->unsignedBigInteger('event_faq_id');
            $table->unsignedBigInteger('supplier_id');
            $table->foreign('event_faq_id')->references('id')->on('event_faqs');
            $table->foreign('supplier_id')->references('id')->on('suppliers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_faq_answers');
    }
}
