<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLotOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lot_offers', function (Blueprint $table) {
            $table->id();
             $table->string('lot_name');
             $table->unsignedBigInteger('supplier_id');
             $table->unsignedBigInteger('event_id');
             $table->foreign('supplier_id')->references('id')->on('suppliers');
             $table->foreign('event_id')->references('id')->on('events');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lot_offers');
    }
}
