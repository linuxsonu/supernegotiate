<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventExcelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_excels', function (Blueprint $table) {
            $table->id();
            $table->string('product_name');
            $table->longText('delivery_location');
            $table->bigInteger('qty');
            $table->bigInteger('unit');
            $table->bigInteger('total_price');
            $table->string('lot_name');
            $table->longText('formula')->nullable();
            $table->longText('shipment_term');
            $table->bigInteger('unit_price');
            $table->timestamps('delivery_date');
            $table->unsignedBigInteger('event_id');
            $table->foreign('event_id')->references('id')->on('events');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_excels');
    }
}
