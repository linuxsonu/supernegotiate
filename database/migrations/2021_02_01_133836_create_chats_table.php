<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chats', function (Blueprint $table) {
            $table->id();
            $table->longText('message');
            $table->longText('notes')->nullable();
            $table->unsignedBigInteger('supplier_id');
            $table->unsignedBigInteger('buyer_id');
            $table->unsignedBigInteger('event_id');
            $table->foreign('buyer_id')->references('id')->on('buyers');
            $table->foreign('event_id')->references('id')->on('events');
            $table->foreign('supplier_id')->references('id')->on('suppliers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chats');
    }
}
