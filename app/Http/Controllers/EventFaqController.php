<?php

namespace App\Http\Controllers;

use App\Models\EventFaq;
use Illuminate\Http\Request;

class EventFaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EventFaq  $eventFaq
     * @return \Illuminate\Http\Response
     */
    public function show(EventFaq $eventFaq)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EventFaq  $eventFaq
     * @return \Illuminate\Http\Response
     */
    public function edit(EventFaq $eventFaq)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EventFaq  $eventFaq
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EventFaq $eventFaq)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EventFaq  $eventFaq
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventFaq $eventFaq)
    {
        //
    }
}
