<?php

namespace App\Http\Controllers;

use App\Models\BuyerSupplier;
use Illuminate\Http\Request;

class BuyerSupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BuyerSupplier  $buyerSupplier
     * @return \Illuminate\Http\Response
     */
    public function show(BuyerSupplier $buyerSupplier)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BuyerSupplier  $buyerSupplier
     * @return \Illuminate\Http\Response
     */
    public function edit(BuyerSupplier $buyerSupplier)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BuyerSupplier  $buyerSupplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BuyerSupplier $buyerSupplier)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BuyerSupplier  $buyerSupplier
     * @return \Illuminate\Http\Response
     */
    public function destroy(BuyerSupplier $buyerSupplier)
    {
        //
    }
}
