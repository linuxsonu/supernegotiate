<?php

namespace App\Http\Controllers;

use App\Models\ChatOffer;
use Illuminate\Http\Request;

class ChatOfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ChatOffer  $chatOffer
     * @return \Illuminate\Http\Response
     */
    public function show(ChatOffer $chatOffer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ChatOffer  $chatOffer
     * @return \Illuminate\Http\Response
     */
    public function edit(ChatOffer $chatOffer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ChatOffer  $chatOffer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ChatOffer $chatOffer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ChatOffer  $chatOffer
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChatOffer $chatOffer)
    {
        //
    }
}
