<?php

namespace App\Http\Controllers;

use App\Models\LotOffer;
use Illuminate\Http\Request;

class LotOfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LotOffer  $lotOffer
     * @return \Illuminate\Http\Response
     */
    public function show(LotOffer $lotOffer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LotOffer  $lotOffer
     * @return \Illuminate\Http\Response
     */
    public function edit(LotOffer $lotOffer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LotOffer  $lotOffer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LotOffer $lotOffer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LotOffer  $lotOffer
     * @return \Illuminate\Http\Response
     */
    public function destroy(LotOffer $lotOffer)
    {
        //
    }
}
