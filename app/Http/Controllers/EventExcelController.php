<?php

namespace App\Http\Controllers;

use App\Models\EventExcel;
use Illuminate\Http\Request;

class EventExcelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EventExcel  $eventExcel
     * @return \Illuminate\Http\Response
     */
    public function show(EventExcel $eventExcel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EventExcel  $eventExcel
     * @return \Illuminate\Http\Response
     */
    public function edit(EventExcel $eventExcel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EventExcel  $eventExcel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EventExcel $eventExcel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EventExcel  $eventExcel
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventExcel $eventExcel)
    {
        //
    }
}
